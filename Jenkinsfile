pipeline {
    agent any
    tools {
        maven "maven_3.6.3"
    }

    stages {
        stage("Checkout"){
            steps{
                checkout([$class: 'GitSCM', branches: [[name: '*/main']], extensions: [], userRemoteConfigs: [[url: 'https://gitlab.com/aldo.lushkja/currency-exchange']]])
            }
        }

        stage("Unit tests") {
            steps {
                script {
                    sh "mvn test"
                }
            }
        }

        stage("Build artifact") {
            steps {
                script {
                    sh "mvn clean package -DskipTests"
                }
            }
        }

        stage("Build docker image") {
            steps {
                script {
                    sh "docker build -t alushkja/currency-exchange ."
                }
            }
        }

        stage("Push image to Hub") {
            steps {
                script {
                    withCredentials([string(credentialsId: 'dockerhub_pwd', variable: 'dockerhub_pwd'), string(credentialsId: 'dockerhub_user', variable: 'dockerhub_user')]) {
                        sh('docker login -u ${dockerhub_user} -p ${dockerhub_pwd}')
                        sh('docker push alushkja/currency-exchange')
                    }
                }
            }
        }
        stage("Run docker container") {
            steps {
                script {
                    sh('docker rm -f currency-exchange')
                    sh('docker run -d --name currency-exchange -p 7000:7000 alushkja/currency-exchange')
                }
            }
        }
    }

}