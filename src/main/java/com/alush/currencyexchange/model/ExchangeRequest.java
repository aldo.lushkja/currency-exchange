package com.alush.currencyexchange.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExchangeRequest {

    @JsonProperty("from")
    @NotBlank(message = "FROM field must be filled correctly")
    private String from;

    @JsonProperty("to")
    @NotBlank(message = "TO field must be filled correctly")
    private String to;

    @JsonProperty("amount")
    @Positive(message = "AMOUNT must be greater than zero")
    private BigDecimal amount;
}
