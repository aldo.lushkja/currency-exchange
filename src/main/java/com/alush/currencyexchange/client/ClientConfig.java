package com.alush.currencyexchange.client;

import feign.RequestInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ClientConfig {

    @Value(value = "${rapid.api_key}")
    private String rapidApiKey;

    @Value(value = "${rapid.host}")
    private String rapidHost;

    @Bean
    public RequestInterceptor requestInterceptor(){
        return requestTemplate -> {
            requestTemplate.header("X-RapidAPI-Key", rapidApiKey);
            requestTemplate.header("X-RapidAPI-Host", rapidHost);
        };
    }

}
