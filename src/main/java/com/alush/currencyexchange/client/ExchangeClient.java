package com.alush.currencyexchange.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "exchange-api", url = "${exchange-api.endpoint}")
public interface ExchangeClient {

    @RequestMapping(method = RequestMethod.GET, value = "/listquotes")
    String listQuotes();

    @RequestMapping(method = RequestMethod.GET, value = "/exchange")
    String getExchangeRate(@RequestParam("to") String to, @RequestParam("to") String from, @RequestParam("q") Double amount);
}
