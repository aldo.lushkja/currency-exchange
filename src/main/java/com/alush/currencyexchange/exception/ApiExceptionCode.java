package com.alush.currencyexchange.exception;

public enum ApiExceptionCode {
    VALIDATION_ERROR(0),APP_ERROR(1),CLIENT_ERROR(2),SERVER_ERROR(3);

    ApiExceptionCode(int errorCode) {
        this.errorCode = errorCode;
    }

    private final int errorCode;

    public int getErrorCode() {
        return errorCode;
    }
}
