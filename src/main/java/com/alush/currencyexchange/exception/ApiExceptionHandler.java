package com.alush.currencyexchange.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    ResponseEntity<ApiException> handleConstraintViolation(MethodArgumentNotValidException violation){
        List<String> errors = new ArrayList<>();
        for (ObjectError objectErrors : violation.getBindingResult().getAllErrors()) {
            final var message = objectErrors.getDefaultMessage();
            errors.add(message);
        }
        return ResponseEntity.status(400).body(new ApiException(ApiExceptionCode.VALIDATION_ERROR.getErrorCode(), errors));
    }
}
