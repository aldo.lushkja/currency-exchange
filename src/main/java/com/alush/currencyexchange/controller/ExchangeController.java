package com.alush.currencyexchange.controller;

import com.alush.currencyexchange.client.ExchangeClient;
import com.alush.currencyexchange.model.ExchangeRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/v1/exchanges/")
@RequiredArgsConstructor
public class ExchangeController {

    private final ExchangeClient exchangeClient;

    @PostMapping(consumes = "application/json")
    public ResponseEntity<String> getExchangeValue(@Valid @RequestBody ExchangeRequest exchangeRequest){
        return ResponseEntity.ok(exchangeClient.getExchangeRate(exchangeRequest.getTo(),exchangeRequest.getFrom(),exchangeRequest.getAmount().doubleValue()));
    }

    @GetMapping("/quotes")
    public ResponseEntity<String> getQuotes(){
        return ResponseEntity.ok(exchangeClient.listQuotes());
    }
}
