FROM amazoncorretto:17-alpine
MAINTAINER Aldo Lushkja <aldo.lushkja@gmail.com>

ADD ./target/currency-exchange.jar /app/
CMD ["java", "-Xmx200m", "-jar", "/app/currency-exchange.jar"]

EXPOSE 7000